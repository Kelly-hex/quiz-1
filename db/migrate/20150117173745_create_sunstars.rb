class CreateSunstars < ActiveRecord::Migration
  def change
    create_table :sunstars do |t|
      t.string :name
      t.string :email
      t.boolean :department
      t.text :message
      t.integer :teb
      t.time :curt

      t.timestamps null: false
    end
  end
end
