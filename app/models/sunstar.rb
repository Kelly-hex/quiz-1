class Sunstar < ActiveRecord::Base

validates :name, presence: true, uniqueness: true
validates :email, presence: true, uniqueness: true, format: {with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i}
validates :message, presence: true 


  end