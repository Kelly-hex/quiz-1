class SunstarsController < ApplicationController
  def index
      @sunstars = Sunstar.all
    end

    def new
      @sunstar = Sunstar.new 
    end

    def create 
      @sunstar = Sunstar.new sunstar_params
     if @sunstar.save
        redirect_to sunstars_path
      else
        render :new
      end
    end
    def edit
      @sunstar = Sunstar.find params[:id]
    end
    def update
    @sunstar = Sunstar.find params[:id]
   # sunstars_params = params.require(:sunstars).permit(:person, :text)
  if @sunstar.update sunstar_params
    redirect_to sunstars_path
    else
    render :edit
  end
end

  def destroy
    @sunstar = Sunstar.find params[:id]
    @sunstar.destroy
    redirect_to sunstars_path
    
  end
    private

    def sunstar_params
      params.require(:sunstar).permit(:email, :message, :name, :department)
    end

end
